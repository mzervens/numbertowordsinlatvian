<?php


class NumberToWords 
{
    private $to19 = array(
                            "nulle",
                            "viens",
                            "divi",
                            "trīs",
                            "četri",
                            "pieci",
                            "seši",
                            "septiņi",
                            "astoņi",
                            "deviņi",
                            "desmit",
                            "vienpadsmit",
                            "divpadsmit",
                            "trīspadsmit",
                            "četrpadsmit",
                            "piecpadsmit",
                            "sešpadsmit",
                            "septiņpadsmit",
                            "astoņpadsmit",
                            "deviņpadsmit"
                          );
    
    
    private $tens = array(
                            "divdesmit",
                            "trīsdesmit",
                            "četrdesmit",
                            "piecdesmit",
                            "sešdesmit",
                            "septiņdesmit",
                            "astoņdesmit",
                            "deviņdesmit"
                        );
    
    
    private $denominations = array(
                                    "",
                                    "tūkstotis",
                                    "miljons",
                                    "miljards",
                                    "triljons",
                                    "kvadriljons",
                                    "kvintiljons"
                                    );
    
    private $denominationsPlural = array(
                                            "",
                                            "tūkstoši",
                                            "miljoni",
                                            "miljardi",
                                            "triljoni",
                                            "kvadriljoni",
                                            "kvintiljoni"
    );
    
    
    private function convertLessThanHundred($number)
    {
        if($number < 20)
        {
            return $this->to19[$number];
        }
        
        for($i = 0; $i < count($this->tens); $i++)
        {
            $val = 20 + ($i * 10);
            $valWord = $this->tens[$i];
            
            if($val <= $number && $number < $val + 10)
            {
                if($number % 10 == 0)
                {
                    return $valWord;
                }
                
                return $valWord . " " . $this->to19[$number % 10];
            }
        }
    }
    
    private function convertLessThanThousand($number)
    {
        $word = "";
        $hundreds = (int)($number / 100);
        $remainder = $number % 100;
        
        if($hundreds > 0 && $remainder == 0)
        {
            $word = $this->to19[$hundreds];
            if($hundreds <= 1)
            {
                $word .= " simts";
            }
            else
            {
                $word .= " simti";
            }
        }
        else
        {
            if($hundreds <= 0)
            {
                $word = $this->convertLessThanHundred($remainder);
            }
            else
            {
                $word = $this->to19[$hundreds];
                if($hundreds <= 1)
                {
                    $word .= " simts ";
                }
                else
                {
                    $word .= " simti ";
                }
                
                $word .= $this->convertLessThanHundred($remainder);
            }
        }
        
        return $word;
        
    }
    
    public function convertToWords($number)
    {
        if($number < 100)
        {
            return $this->convertLessThanHundred($number);
        }
        else if($number < 1000)
        {
            return $this->convertLessThanThousand($number);
        }
        
        for($i = 0; $i < count($this->denominations); $i++)
        {
            $denomVal = pow(1000, $i);
            $prevIndx = $i - 1;
            
            if($denomVal > $number)
            {                
                $mod = pow(1000, $prevIndx);
                $left = (int)($number / $mod);
                $right = $number - ($left * $mod);
                
                $output = $this->convertLessThanThousand($left);
                
                if($left <= 1)
                {
                    $output .= " " . $this->denominations[$prevIndx];
                }
                else
                {
                    $output .= " " . $this->denominationsPlural[$prevIndx];
                }
                
                
                if($right > 0)
                {
                    $output .= " " . $this->convertToWords($right);
                }
                
                return $output;
            }
            
            
        }
        
        
    }
    
    
}
